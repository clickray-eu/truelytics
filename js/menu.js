$(document).ready(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var h = $(".header");
        if(scroll > 10) {
            h.addClass('header--dark');
        } else {
            h.removeClass('header--dark');
        }
    })
})